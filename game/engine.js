const maxX = 44;
const maxY = 25;
const cellSize = 33;
const NUMBER_OF_ENEMIES = 20;
const ENEMY_STARTER = 5000;
const SPEED_OF_ENEMY = 0.05;
const ENEMY_LIFE = 5; //5
const ENEMY_REWARD = 10;
const ENEMY_SCORE = 23;
const TOWER_PRICEBASE = 40;

/*
  TODO: ENEMY LIFE, PROPERTY, MENNYISÉG, IDŐ BEÁLLÍTÁSA RENDESRE
  ÚT A PÁLYÁRA BERAJZOLNI.
  JOBB KLIKK NEM WAVE, CSAK 1 ENEMY-T
  editorba grass

*/

const POISONTOWER_PRICE = Math.floor(TOWER_PRICEBASE);
const ICETOWER_PRICE = Math.floor(TOWER_PRICEBASE) * 3;
const CANNONTOWER_PRICE = Math.floor(TOWER_PRICEBASE) * 7;
const FLAMETOWER_PRICE = Math.floor(TOWER_PRICEBASE) * 11;

function makeGameHarder(gameState) {
  if (gameState.difficulty < 10) {
    gameState.difficulty = gameState.difficulty * 1.1;
  }
}

function makeGameEasier(gameState) {
  if (gameState.difficulty > 0.5) {
    gameState.difficulty = gameState.difficulty * 0.9;
  }
}

function setup(editor) {
  let gameState;
  if (!editor) {
    gameState = {
      difficulty: 1,
      editor: editor,
      onplay: true,
      livesRemaining: 10,
      money: 1000,
      score: 0,
      startPoint: { gridX: 0, gridY: 0 },
      endPoint: { gridX: maxX - 1, gridY: maxY - 1 },
      selectedTower: null,
      selectedObstacle: null,
      selectedRoad: null,
      selectedObject: null,
      towerList: [],
      enemyList: [],
      bulletList: [],
      deadList: [],
      corpseList: [],
      playdeadsList: [],
      obstacleList: [
        { name: 'rock', randomRockCol: 3, randomRockRow: 2, gridX: 4, gridY: 12 },
        { name: 'rock', randomRockCol: 3, randomRockRow: 2, gridX: 4, gridY: 12 },
        { name: 'rock', randomRockCol: 4, randomRockRow: 0, gridX: 5, gridY: 12 },
        { name: 'rock', randomRockCol: 4, randomRockRow: 0, gridX: 4, gridY: 10 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 4, gridY: 9 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 4, gridY: 8 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 11, gridY: 15 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 11, gridY: 14 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 11, gridY: 13 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 25, gridY: 22 },
        { name: 'rock', randomRockCol: 1, randomRockRow: 0, gridX: 25, gridY: 21 },
        { name: 'rock', randomRockCol: 0, randomRockRow: 2, gridX: 25, gridY: 20 },
        { name: 'rock', randomRockCol: 5, randomRockRow: 2, gridX: 25, gridY: 19 },
        { name: 'rock', randomRockCol: 3, randomRockRow: 0, gridX: 25, gridY: 18 },
        { name: 'rock', randomRockCol: 3, randomRockRow: 2, gridX: 25, gridY: 17 },
        { name: 'rock', randomRockCol: 3, randomRockRow: 1, gridX: 25, gridY: 16 },
        { name: 'rock', randomRockCol: 5, randomRockRow: 1, gridX: 23, gridY: 15 },
        { name: 'rock', randomRockCol: 5, randomRockRow: 1, gridX: 23, gridY: 14 },

        { name: 'water', gridX: 4, gridY: 15 },
        { name: 'water', gridX: 5, gridY: 15 },
        { name: 'water', gridX: 5, gridY: 14 },
        { name: 'water', gridX: 4, gridY: 14 },
        { name: 'water', gridX: 4, gridY: 13 },
        { name: 'water', gridX: 5, gridY: 13 },
        { name: 'water', gridX: 7, gridY: 14 },
        { name: 'water', gridX: 7, gridY: 15 },
        { name: 'water', gridX: 7, gridY: 16 },
        { name: 'water', gridX: 9, gridY: 16 },
        { name: 'water', gridX: 8, gridY: 16 },
        { name: 'water', gridX: 8, gridY: 15 },
        { name: 'water', gridX: 9, gridY: 15 },
        { name: 'water', gridX: 9, gridY: 14 },
        { name: 'water', gridX: 8, gridY: 14 },
        { name: 'water', gridX: 26, gridY: 22 },
        { name: 'water', gridX: 26, gridY: 21 },
        { name: 'water', gridX: 26, gridY: 20 },
        { name: 'water', gridX: 26, gridY: 19 },
        { name: 'water', gridX: 26, gridY: 18 },
        { name: 'water', gridX: 26, gridY: 17 },
        { name: 'water', gridX: 26, gridY: 17 },
        { name: 'water', gridX: 26, gridY: 16 },
        { name: 'water', gridX: 24, gridY: 15 },
        { name: 'water', gridX: 25, gridY: 15 },
        { name: 'water', gridX: 26, gridY: 15 },
        { name: 'water', gridX: 24, gridY: 14 },
        { name: 'water', gridX: 23, gridY: 13 },
        { name: 'water', gridX: 22, gridY: 12 },
        { name: 'water', gridX: 21, gridY: 11 },
        { name: 'water', gridX: 22, gridY: 13 },
        { name: 'water', gridX: 23, gridY: 12 },
        { name: 'water', gridX: 24, gridY: 13 },
        { name: 'water', gridX: 21, gridY: 11 },
        { name: 'water', gridX: 21, gridY: 12 },
        { name: 'water', gridX: 22, gridY: 11 },
        { name: 'water', gridX: 22, gridY: 9 },
        { name: 'water', gridX: 22, gridY: 10 },
        { name: 'water', gridX: 22, gridY: 8 },
        { name: 'water', gridX: 22, gridY: 7 },
        { name: 'water', gridX: 21, gridY: 7 },
        { name: 'water', gridX: 21, gridY: 8 },
        { name: 'water', gridX: 18, gridY: 4 },
        { name: 'water', gridX: 17, gridY: 3 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 26, gridY: 14 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 25, gridY: 14 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 25, gridY: 13 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 24, gridY: 12 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 23, gridY: 11 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 23, gridY: 10 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 1, gridX: 19, gridY: 3 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 1, gridX: 18, gridY: 3 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 1, gridX: 23, gridY: 9 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 33, gridY: 13 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 34, gridY: 15 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 21, gridY: 9 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 0, gridX: 20, gridY: 10 },
        { name: 'tree', randomRockCol: 0, randomRockRow: 2, gridX: 10, gridY: 11 },
        { name: 'tree', randomRockCol: 0, randomRockRow: 2, gridX: 9, gridY: 10 },
        { name: 'tree', randomRockCol: 0, randomRockRow: 2, gridX: 0, gridY: 1 },
        { name: 'tree', randomRockCol: 0, randomRockRow: 2, gridX: 0, gridY: 2 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 1, gridX: 9, gridY: 0 },
        { name: 'tree', randomRockCol: 1, randomRockRow: 1, gridX: 0, gridY: 12 },
      ],
      roadList: [
        { name: 'road', gridX: 11, gridY: 1 },
        { name: 'road', gridX: 11, gridY: 2 },
        { name: 'road', gridX: 11, gridY: 3 },
        { name: 'road', gridX: 11, gridY: 4 },
        { name: 'road', gridX: 11, gridY: 5 },
        { name: 'road', gridX: 11, gridY: 6 },
        { name: 'road', gridX: 11, gridY: 7 },
        { name: 'road', gridX: 11, gridY: 8 },
        { name: 'road', gridX: 11, gridY: 9 },
        { name: 'road', gridX: 11, gridY: 10 },
        { name: 'road', gridX: 11, gridY: 11 },
        { name: 'road', gridX: 11, gridY: 12 },
        { name: 'road', gridX: 12, gridY: 12 },
      ],
      lastUpdate: Date.now(),
      gameStartTime: Date.now(),
      gameTime: 0,
      waves: [
        {
          beginTime: 100,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'scorpion',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 7,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 1,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredScorpion',
            life: ENEMY_LIFE * 10,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 2,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenOrc',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 3,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'stoneOgre',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 4,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'axeOrc',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 5,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blackWizard',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 7,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredOrc',
            life: ENEMY_LIFE * 6,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 8,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blueDevil',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 6,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 9,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blackDevil',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 10,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'ghost',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 8,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 11,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'redZombie',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 12,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenZombie',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 13,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredDevil',
            life: ENEMY_LIFE * 5,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 14,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'gnome',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 10,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 15,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'purpleImp',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 7,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 16,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'whiteDiver',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 17,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'axeman',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 18,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenDevil',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 8,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 19,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greyOgre',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 20,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'runner',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 10,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 21,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'jumper',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 9,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
      ],
    };
  } else {
    gameState = {
      difficulty: 1,
      editor: editor,
      onplay: true,
      livesRemaining: 10,
      money: 1000,
      score: 0,
      startPoint: { gridX: 0, gridY: 0 },
      endPoint: { gridX: maxX - 1, gridY: maxY - 1 },
      selectedTower: null,
      selectedObstacle: null,
      selectedRoad: null,
      towerList: [],
      towerList: [],
      enemyList: [],
      bulletList: [],
      deadList: [],
      corpseList: [],
      playdeadsList: [],
      obstacleList: [],
      roadList: [],

      lastUpdate: Date.now(),
      gameStartTime: Date.now(),
      paused: false,
      gameTime: 0,
      waves: [
        {
          beginTime: 100,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'scorpion',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 7,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 1,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredScorpion',
            life: ENEMY_LIFE * 10,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 2,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 2,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenOrc',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 3,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'stoneOgre',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },
          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 4,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'axeOrc',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 5,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blackWizard',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 7,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredOrc',
            life: ENEMY_LIFE * 6,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 8,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blueDevil',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 6,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 9,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'blackDevil',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 10,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'ghost',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 8,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 11,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'redZombie',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 12,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenZombie',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 13,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'armoredDevil',
            life: ENEMY_LIFE * 5,
            speed: SPEED_OF_ENEMY * 3,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 14,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'gnome',
            life: ENEMY_LIFE * 1,
            speed: SPEED_OF_ENEMY * 10,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 15,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'purpleImp',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 7,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 16,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'whiteDiver',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 5,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },

        {
          beginTime: ENEMY_STARTER * 17,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'axeman',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 18,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greenDevil',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 8,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 19,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'greyOgre',
            life: ENEMY_LIFE * 4,
            speed: SPEED_OF_ENEMY * 4,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 20,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'runner',
            life: ENEMY_LIFE * 2,
            speed: SPEED_OF_ENEMY * 10,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
        {
          beginTime: ENEMY_STARTER * 21,
          enemiesCounter: NUMBER_OF_ENEMIES,
          enemy: {
            type: 'jumper',
            life: ENEMY_LIFE * 3,
            speed: SPEED_OF_ENEMY * 9,
            size: cellSize / 4,
            reward: ENEMY_REWARD,
            score: ENEMY_SCORE * 1,
          },

          repeatTime: 300,
          lastDeployTime: 0,
        },
      ],
    };
  }
  gameState.waves.forEach((wave) => {
    wave.enemy.maxLife = wave.enemy.life;
  });

  function saveJsonObjToFile(saveObj) {
    // file setting
    const text = JSON.stringify(saveObj);
    const name = 'towerdefensesavegame.json';
    const type = 'text/plain';

    // create file
    const a = document.createElement('a');
    const file = new Blob([text], { type: type });
    a.href = URL.createObjectURL(file);
    a.download = name;
    document.body.appendChild(a);
    a.click();
    a.remove();
  }

  function openFileDialog(accept, multy = false, callback) {
    let inputElement = document.createElement('input');
    inputElement.type = 'file';
    inputElement.accept = accept; // Note Edge does not support this attribute
    if (multy) {
      inputElement.multiple = multy;
    }
    if (typeof callback === 'function') {
      inputElement.addEventListener('change', callback);
    }
    inputElement.dispatchEvent(new MouseEvent('click'));
  }

  // userButton click event
  function openDialogClick() {
    // open file dialog for text files
    openFileDialog('.json', true, readSingleFile);
  }

  function readSingleFile(e) {
    let file = e.target.files[0];
    if (!file) {
      return;
    }
    let reader = new FileReader();
    reader.onload = function (e) {
      var contents = e.target.result;
      let loadedGameState;
      //console.log(contents);
      loadedGameState = contents;
      Object.assign(gameState, JSON.parse(loadedGameState));
      gameState.gameStartTime = Date.now();
      gameState.lastUpdate = Date.now();
    };
    reader.readAsText(file);
  }

  document.getElementById('makeGameSpeedSlower').addEventListener('click', makeGameSpeedSlower());
  document.getElementById('makeGameSpeedFaster').addEventListener('click', makeGameSpeedFaster());

  let easier = document.getElementById('makeGameEasier');
  let harder = document.getElementById('makeGameHarder');

  if (easier) {
    easier.addEventListener('click', () => makeGameEasier(gameState));
  }
  if (harder) {
    harder.addEventListener('click', () => makeGameHarder(gameState));
  }
  document.getElementById('gamePlayPause').addEventListener('click', gamePlayPause(gameState.onplay));

  document.getElementById('load').addEventListener('click', openDialogClick);

  document.getElementById('gamePlayPause').addEventListener('click', function () {
    showPlayToStart(timer_is_on, editor);
    gamePlayPause(gameState.onplay);
  });

  document.getElementById('save').addEventListener('click', function () {
    if (!editor) {
      saveJsonObjToFile(gameState);
    }
    if (editor) {
      saveJsonObjToFile(gameState);
      //localStorage.setItem('saveEditor', JSON.stringify(gameState));
    }
  });

  document.getElementById('poisonTower').addEventListener('click', function () {
    let poisonPrice = Math.floor(POISONTOWER_PRICE * gameState.difficulty);
    gameState.tower = {
      towerName: 'poisonTower',
      range: 4,
      reloadingTime: 200,
      timeToReload: 0,
      price: poisonPrice,
    };
    gameState.selectedButton = 'tower';
  });

  document.getElementById('cannonTower').addEventListener('click', function () {
    let cannonTowerPrice = Math.floor(CANNONTOWER_PRICE * gameState.difficulty);
    gameState.tower = {
      towerName: 'cannonTower',
      range: 8,
      reloadingTime: 250,
      timeToReload: 0,
      price: cannonTowerPrice,
    };
    gameState.selectedButton = 'tower';
  });

  document.getElementById('iceTower').addEventListener('click', function () {
    let iceTowerPrice = Math.floor(ICETOWER_PRICE * gameState.difficulty);
    gameState.tower = {
      towerName: 'iceTower',
      range: 5,
      reloadingTime: 150,
      timeToReload: 0,
      price: iceTowerPrice,
    };
    gameState.selectedButton = 'tower';
  });

  document.getElementById('flameTower').addEventListener('click', function () {
    let flameTowerPrice = Math.floor(FLAMETOWER_PRICE * gameState.difficulty);
    gameState.tower = {
      towerName: 'flameTower',
      range: 3,
      reloadingTime: 20,
      timeToReload: 0,
      price: flameTowerPrice,
    };
    gameState.selectedButton = 'tower';
  });

  enemiesOnGame = [
    {
      type: 'armoredScorpion',
      life: ENEMY_LIFE * 20,
      speed: SPEED_OF_ENEMY * 5,
      size: cellSize / 2,
      reward: ENEMY_REWARD,
      score: ENEMY_SCORE * 1,
    },

    {
      type: 'armoredOrc',
      life: ENEMY_LIFE * 16,
      speed: SPEED_OF_ENEMY * 3,
      size: cellSize / 2,
      reward: ENEMY_REWARD,
      score: ENEMY_SCORE * 1,
    },

    {
      type: 'blueDevil',
      life: ENEMY_LIFE * 11,
      speed: SPEED_OF_ENEMY * 6,
      size: cellSize / 2,
      reward: ENEMY_REWARD,
      score: ENEMY_SCORE * 1,
    },

    {
      type: 'armoredDevil',
      life: ENEMY_LIFE * 15,
      speed: SPEED_OF_ENEMY * 3,
      size: cellSize / 2,
      reward: ENEMY_REWARD,
      score: ENEMY_SCORE * 1,
    },
  ];

  let enemyAdderButton = document.getElementById('ogreButton');

  document.getElementById('ogreButton').addEventListener('click', function () {
    console.log('CLICKED');

    let randomEnemyIndex = randomizator(enemiesOnGame.length);
    let newEnemy = makeEnemy({ x: 1, y: 1 }, 0.5, cellSize / 2, (Math.PI * 3) / 4);
    placeWave(gameState, { x: 1, y: 1 });

    newEnemy.alive = true;
    newEnemy.playdead = false;
    newEnemy.frameXCounter = 0;
    newEnemy.frameYCounter = 0;
    newEnemy.deadAnimationCounter = 0;
    newEnemy.type = enemiesOnGame[randomEnemyIndex].type;
    newEnemy.life = enemiesOnGame[randomEnemyIndex].life;
    newEnemy.speed = enemiesOnGame[randomEnemyIndex].speed;
    newEnemy.size = enemiesOnGame[randomEnemyIndex].size;
    newEnemy.reward = enemiesOnGame[randomEnemyIndex].reward;
    newEnemy.score = enemiesOnGame[randomEnemyIndex].score;

    let newWave = makeWave(gameState.gameTime + 1000, NUMBER_OF_ENEMIES, newEnemy, 300, 0);

    gameState.waves.push(newWave);

    enemyAdderButton.style.display = 'none';

    ogreButton = false;
    processingWave = true;

    gameState.remainingEnemies += newWave.enemiesCounter;
  });

  document.getElementById('road').addEventListener('click', function () {
    gameState.road = {
      name: 'road',
    };
    gameState.selectedButton = 'road';
  });

  document.getElementById('rock').addEventListener('click', function () {
    let randomC = randomizator(rock.column);
    let randomR = randomizator(rock.row);

    gameState.obstacle = {
      name: 'rock',
      randomRockCol: randomC,
      randomRockRow: randomR,
    };
    gameState.selectedButton = 'rock';
  });

  document.getElementById('tree').addEventListener('click', function () {
    let randomC = randomizator(tree.column);
    let randomR = randomizator(tree.row);

    gameState.obstacle = {
      name: 'tree',
      randomRockCol: randomC,
      randomRockRow: randomR,
    };
    gameState.selectedButton = 'tree';
  });

  document.getElementById('water').addEventListener('click', function () {
    gameState.obstacle = {
      name: 'water',
    };
    gameState.selectedButton = 'water';
  });

  document.getElementById('demolish').addEventListener(
    'click',
    function () {
      if (gameState.selectedObject.type === 'tower') {
        let filtered = gameState.towerList.filter((tower) => {
          return tower !== gameState.selectedTower;
        });
        gameState.towerList = filtered;

        if (!editor) {
          gameState.enemyList.forEach((enemy) => {
            makeEnemyPath(enemy, gameState.towerList, gameState.roadList, gameState.obstacleList, gameState.endPoint);
            calculateEnemyDirection(enemy);
          });
        }
      }

      if (editor) {
        if (
          gameState.selectedObject.type === 'rock' ||
          gameState.selectedObject.type === 'water' ||
          gameState.selectedObject.type === 'tree'
        ) {
          let filtered = gameState.obstacleList.filter((obstacle) => {
            return obstacle !== gameState.selectedObject;
          });
          gameState.obstacleList = filtered;
        }
        if (gameState.selectedObject.type === 'road') {
          let filtered = gameState.roadList.filter((road) => {
            return road !== gameState.selectedObject;
          });
          gameState.roadList = filtered;
        }
      }
    },
    false,
  );

  const canvas = document.getElementById('myCanvas');
  canvas.width = maxX * cellSize;
  canvas.height = maxY * cellSize;

  startCounter(10, gameState, canvas, editor);
  //jobb klikk editoros ellenségberakás.... ha marad idő.
  if (!editor) {
    //tesztelés
    canvas.addEventListener(
      'contextmenu',
      function (click) {
        let cellcoord = coordsToGrid(click.offsetX, click.offsetY);
        let newEnemy = makeEnemy(click.offsetX, click.offsetY, 0.5, cellSize / 2, (Math.PI * 3) / 4);

        placeWave(gameState, cellcoord);

        newEnemy.alive = true;
        newEnemy.playdead = false;
        newEnemy.frameXCounter = 0;
        newEnemy.frameYCounter = 0;
        newEnemy.deadAnimationCounter = 0;
        newEnemy.type = 'armoredScorpion';
        newEnemy.life = ENEMY_LIFE * 30;
        newEnemy.speed = SPEED_OF_ENEMY * 4;
        newEnemy.size = cellSize / 2;
        newEnemy.reward = ENEMY_REWARD;
        newEnemy.score = ENEMY_SCORE * 1;

        let newWave = makeWave(gameState.gameTime + 1000, NUMBER_OF_ENEMIES, newEnemy, 300, 0);
        makeEnemyPath(newEnemy, gameState.towerList, gameState.roadList, gameState.obstacleList, gameState.endPoint);
        calculateEnemyDirection(newEnemy);
        gameState.waves.push(newWave);

        click.preventDefault();
        return false;
      },
      false,
    );
  }

  canvas.addEventListener(
    'click',
    function (click) {
      let cellcoord = coordsToGrid(click.offsetX, click.offsetY);
      if (gameState.selectedButton === 'tower') {
        placeTower(gameState, cellcoord);
        gameState.towerList.forEach((tower) => {
          if (tower.gridX == cellcoord.gridX && tower.gridY == cellcoord.gridY) {
            gameState.selectedTower = tower;
            gameState.selectedObject = tower;
            gameState.selectedObject = tower;
            gameState.selectedObject.type = 'tower';
            console.log(tower);
          }
        });
      }
      if (gameState.selectedButton === 'rock') {
        placeObstacle(gameState, cellcoord);
        gameState.obstacleList.forEach((obstacle) => {
          if (obstacle.gridX == cellcoord.gridX && obstacle.gridY == cellcoord.gridY) {
            gameState.cellcoord = obstacle;
            gameState.selectedObstacle = obstacle;
            gameState.selectedObject = obstacle;
            gameState.selectedObject.type = 'rock';
          }
        });
      }

      if (gameState.selectedButton === 'water') {
        placeObstacle(gameState, cellcoord);
        gameState.obstacleList.forEach((obstacle) => {
          if (obstacle.gridX == cellcoord.gridX && obstacle.gridY == cellcoord.gridY) {
            gameState.cellcoord = obstacle;
            gameState.selectedObstacle = obstacle;
            gameState.selectedObject = obstacle;
            gameState.selectedObject.type = 'water';
          }
        });
      }
      if (gameState.selectedButton === 'tree') {
        placeObstacle(gameState, cellcoord);
        gameState.obstacleList.forEach((obstacle) => {
          if (obstacle.gridX == cellcoord.gridX && obstacle.gridY == cellcoord.gridY) {
            gameState.cellcoord = obstacle;
            gameState.selectedObstacle = obstacle;
            gameState.selectedObject = obstacle;
            gameState.selectedObject.type = 'tree';
          }
        });
      }

      if (gameState.selectedButton === 'road') {
        placeRoad(gameState, cellcoord);
        gameState.roadList.forEach((road) => {
          if (road.gridX == cellcoord.gridX && road.gridY == cellcoord.gridY) {
            gameState.cellcoord = road;
            gameState.selectedRoad = road;
            gameState.selectedObject = road;
            gameState.selectedObject.type = 'road';
          }
        });
      } else {
        gameState.towerList.forEach((tower) => {
          if (tower.gridX == cellcoord.gridX && tower.gridY == cellcoord.gridY) {
            gameState.selectedTower = tower;
            gameState.selectedObject = tower;
            return;
          }
        });

        gameState.roadList.forEach((road) => {
          if (road.gridX == cellcoord.gridX && road.gridY == cellcoord.gridY) {
            gameState.selectedRoad = road;
            gameState.selectedObject = road;
            return;
          }
        });

        gameState.obstacleList.forEach((obstacle) => {
          if (obstacle.gridX == cellcoord.gridX && obstacle.gridY == cellcoord.gridY) {
            gameState.selectedObstacle = obstacle;
            gameState.selectedObject = obstacle;
            return;
          }
        });
      }
    },
    false,
  );

  if (!editor) {
    //let input = document.cookie;
    let split = document.cookie.split('=');
    let userName = split[1];

    gameState.name = userName;
    gameState.lastUpdate = Date.now();
  }
  gameState.remainingEnemies = 0;
  gameState.waves.forEach((wave) => {
    gameState.remainingEnemies += wave.enemiesCounter;
  });

  function gamePlayPause(onplay) {
    if (!timer_is_on) {
      clearTimeout(t);
      timer_is_on = 1;
      onplay = true;
    } else {
      clearTimeout(t);
      timer_is_on = 0;
      onplay = false;
    }
  }
} //end of setup

function enemiesStillOnStage(endPoint, enemyList) {
  let endPointCoords = gridToCoordsCenter(endPoint.gridX, endPoint.gridY);
  return enemyList.filter(
    (enemy) =>
      !(Math.abs(enemy.x - endPointCoords.x) < enemy.size && Math.abs(enemy.y - endPointCoords.y) < enemy.size),
  );
}

let enemyAdderButton = document.getElementById('ogreButton');
enemyAdderButton.style.display = 'none';

function update(gameState, canvas, elapsedTime) {
  var waveComes = false;
  gameState.waves.forEach((wave) => {
    let endTime = wave.beginTime + wave.repeatTime * wave.enemiesCounter;
    waveComes = waveComes || (gameState.gameTime > wave.beginTime && gameState.gameTime < endTime);
  });

  if (!waveComes && gameState.enemyList.length === 0) {
    enemyAdderButton.style.display = 'block';
    ogreButton = true;
  }

  gameState.gameTime += elapsedTime;
  moveEnemy(gameState.enemyList, elapsedTime);
  gameState.bulletList = moveBullet(gameState.bulletList, elapsedTime);
  let { survivorList, deadList, corpseList, remainingBulletList } = checkShoot(
    gameState.enemyList,
    gameState.deadList,
    gameState.corpseList,
    gameState.bulletList,
  );
  let kills = gameState.enemyList.length - survivorList.length;
  gameState.enemyList = survivorList;
  gameState.bulletList = remainingBulletList;

  for (let i = 0; i < corpseList.length; ++i) {
    let deadType = corpseList[i].type;
    let corpsePrice = corpseList[i].reward;
    let corpseScore = deadList[i].score;
    gameState.deadList.push(deadList[i]);
    switch (deadType) {
      case 'scorpion':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'armoredScorpion':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'axeOrc':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'greenOrc':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'stoneOgre':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'blackWizard':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'armoredOrc':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'blueDevil':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'blackDevil':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'ghost':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'redZombie':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'greenZombie':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'armoredDevil':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'gnome':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'purpleImp':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'whiteDiver':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'axeman':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'greenDevil':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'greyOgre':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'runner':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
      case 'jumper':
        gameState.money += corpsePrice;
        gameState.score += corpseScore;
        break;
    }
  }

  bulletShoot(gameState.towerList, gameState.enemyList, elapsedTime, gameState.bulletList);
  if (gameState.onplay) {
    createWave(
      gameState.waves,
      gameState.gameTime,
      gameState.enemyList,
      gameState.towerList,
      gameState.roadList,
      gameState.obstacleList,
      gameState.startPoint,
      gameState.endPoint,
      gameState.remainingWaves,
    );
  }
  gameState.bulletList = visibleBullets(gameState.bulletList, canvas);
  let playingEnemies = enemiesStillOnStage(gameState.endPoint, gameState.enemyList);

  let livesLost = gameState.enemyList.length - playingEnemies.length;
  gameState.enemyList = playingEnemies;
  gameState.remainingEnemies -= kills;
  gameState.livesRemaining -= livesLost;
  if (gameState.remainingEnemies < 1) {
    let canv = document.getElementById('myCanvas');
    canv.remove();
    gameState.onplay = false;
    document.getElementById('winner').style.display = 'block';
    cancelAnimationFrame(() => animate(gameState, canvas, editor, fps));
  }

  if (gameState.livesRemaining < 1) {
    let canv = document.getElementById('myCanvas');
    canv.remove();
    gameState.onplay = false;

    document.getElementById('gameover').style.display = 'block';
  }

  for (let i = 0; i < gameState.deadList.length; ++i) {
    if (gameState.deadList[i].playdead === true) {
      gameState.deadList.splice(i, 1);
    }
  }
}

function visibleBullets(bulletList, canvas) {
  return bulletList.filter(
    (bullet) =>
      bullet.x > -bullet.size &&
      bullet.y > -bullet.size &&
      bullet.x < canvas.width + bullet.size &&
      bullet.y < canvas.height + bullet.size,
  );
}

function createWave(waves, gameTime, enemyList, towerList, roadList, obstacleList, startPoint, endPoint) {
  waves.forEach((wave) => {
    if (gameTime > wave.beginTime && gameTime < wave.beginTime + wave.enemiesCounter * wave.repeatTime) {
      if (wave.lastDeployTime < gameTime - wave.repeatTime) {
        let startCoordinate = gridToCoordsCenter(startPoint.gridX, startPoint.gridY);
        let enemy = { ...wave.enemy };
        enemy.x = startCoordinate.x;
        enemy.y = startCoordinate.y;
        enemy.alive = true; ////////
        enemy.playdead = false;
        enemy.frameXCounter = 0;
        enemy.frameYCounter = 0;
        enemy.deadAnimationCounter = 0;
        enemyList.push(enemy);
        makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint);
        wave.lastDeployTime = gameTime;
      }
    }
  });
}

function makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint) {
  let enemyGrid = coordsToGrid(enemy.x, enemy.y);
  enemy.path = callAStar(towerList, roadList, obstacleList, maxX, maxY, enemyGrid, endPoint);
}

function callAStar(towerList, roadList, obstacleList, maxX, maxY, startPoint, endPoint) {
  let stage = [];
  for (let x = 0; x < maxX; x++) {
    let row = [];
    for (let y = 0; y < maxY; y++) {
      row.push({
        x: x,
        y: y,
        f: Number.MAX_SAFE_INTEGER,
        g: Number.MAX_SAFE_INTEGER,
        free: true,
        road: false,
      });
    }
    stage.push(row);
  }
  towerList.forEach((tower) => {
    stage[tower.gridX][tower.gridY].free = false;
  });
  roadList.forEach((road) => {
    stage[road.gridX][road.gridY].road = true;
  });
  obstacleList.forEach((obstacle) => {
    stage[obstacle.gridX][obstacle.gridY].free = false;
  });
  let aStar = aStarPathFinder(stage, stage[startPoint.gridX][startPoint.gridY], stage[endPoint.gridX][endPoint.gridY]);
  if (aStar === null) {
    return null;
  }
  let aStarVariable = aStar.map(function (c) {
    return { gridX: c.x, gridY: c.y };
  });
  return aStarVariable;
}

function moveEnemy(enemyList, elapsedTime) {
  enemyList.forEach((enemy) => {
    if (enemy.path.length > 0) {
      let currentStepGridCenter = gridToCoordsCenter(enemy.path[0].gridX, enemy.path[0].gridY);
      if (Math.abs(enemy.x - currentStepGridCenter.x) < 10 && Math.abs(enemy.y - currentStepGridCenter.y) < 10) {
        enemy.path.shift();
        calculateEnemyDirection(enemy);
      }
    }

    move(enemy, elapsedTime);
  });
}

function calculateEnemyDirection(enemy) {
  if (enemy.path.length === 0) {
    return;
  }
  let nextStepGrid = gridToCoordsCenter(enemy.path[0].gridX, enemy.path[0].gridY);
  enemy.direction = Math.atan2(nextStepGrid.y - enemy.y, nextStepGrid.x - enemy.x) + Math.PI / 2;
}

function moveBullet(bulletList, elapsedTime) {
  bulletList.forEach((bullet) => {
    move(bullet, elapsedTime);
  });
  return bulletList.filter((bullet) => rangeDistance({ x: bullet.sx, y: bullet.sy }, bullet) < bullet.range * cellSize);
}

function bulletShoot(towerList, enemyList, elapsedTime, bulletList) {
  towerList.forEach((tower) => {
    tower.timeToReload -= elapsedTime;
    if (tower.timeToReload <= 0) {
      if (enemyList.length > 0) {
        let target = closestEnemy(enemyList, tower);
        if (target !== null) {
          shoot(target, bulletList, tower);
          tower.timeToReload = tower.reloadingTime;
        }
      }
    }
  });
}

function closestEnemy(enemyList, tower) {
  let actualTowerCoord = gridToCoordsCenter(tower.gridX, tower.gridY);
  let enemyOnFire = enemyList.filter((enemy) => {
    return rangeDistance(actualTowerCoord, enemy) < Math.floor(tower.range * cellSize);
  });
  if (enemyOnFire.length === 0) {
    return null;
  } else {
    return enemyOnFire[0];
  }
}

function rangeDistance(a, b) {
  let dX = Math.abs(a.x - b.x);
  let dY = Math.abs(a.y - b.y);
  let newDistance = Math.sqrt(dX * dX + dY * dY);
  return newDistance;
}

function shoot(target, bulletList, tower) {
  let towerType = tower.towerName;

  let bullet = {
    x: tower.gridX * cellSize + cellSize / 2,
    y: tower.gridY * cellSize + cellSize / 2,
    speed: 0.5,
    sx: tower.gridX * cellSize + cellSize / 2,
    sy: tower.gridY * cellSize + cellSize / 2,
    direction: Math.atan2(tower.gridY * cellSize - target.y, tower.gridX * cellSize - target.x) - Math.PI / 2,
    size: cellSize,
    frameXCounter: 0,
  };
  switch (towerType) {
    case 'flameTower':
      bullet.type = 'fireball';
      bullet.range = tower.range;
      break;
    case 'cannonTower':
      bullet.type = 'cannonball';
      bullet.range = tower.range * 2;
      break;
    case 'iceTower':
      bullet.type = 'iceball';
      bullet.range = tower.range;
      break;
    case 'poisonTower':
      bullet.type = 'poisonball';
      bullet.range = tower.range;
      break;
  }

  bulletList.push(bullet);
}

function move(object, elapsedTime) {
  let dx = object.speed * elapsedTime * Math.cos(object.direction - Math.PI / 2);
  let dy = object.speed * elapsedTime * Math.sin(object.direction - Math.PI / 2);

  object.x += dx;
  object.y += dy;
}

function coordsToGrid(x, y) {
  return { gridX: Math.floor(x / cellSize), gridY: Math.floor(y / cellSize) };
}

function gridToCoordsCenter(gridX, gridY) {
  return {
    x: gridX * cellSize + cellSize / 2,
    y: gridY * cellSize + cellSize / 2,
  };
}

function placeTower(gameState, point) {
  let { towerList, roadList, obstacleList, enemyList, startPoint, endPoint } = gameState;
  if (!isGridFree([...towerList, ...roadList, ...obstacleList], enemyList, startPoint, endPoint, point)) {
    return;
  }
  let tower = { ...gameState.tower, gridX: point.gridX, gridY: point.gridY };
  if (gameState.tower) {
    if (callAStar([...towerList, tower], [], [], maxX, maxY, startPoint, endPoint) === null) {
      return;
    }
    if (gameState.money >= tower.price) {
      towerList.push(tower);
      enemyList.forEach((enemy) => {
        makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint);
        calculateEnemyDirection(enemy);
      });
      let towerName = tower.towerName;
      console.log(gameState.editor);
      if (!gameState.editor) {
        switch (towerName) {
          case 'poisonTower':
            gameState.money -= tower.price;
            break;
          case 'cannonTower':
            gameState.money -= tower.price;
            break;
          case 'iceTower':
            gameState.money -= tower.price;
            break;
          case 'flameTower':
            gameState.money -= tower.price;
            break;
        }
      }
    }
  }
}

function placeRoad(gameState, point) {
  let { towerList, roadList, obstacleList, enemyList, startPoint, endPoint } = gameState;
  if (!isGridFree([...towerList, ...roadList, ...obstacleList], enemyList, startPoint, endPoint, point)) {
    return;
  }
  let road = { ...gameState.roadList, gridX: point.gridX, gridY: point.gridY };
  roadList.push(road);
  enemyList.forEach((enemy) => {
    makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint);
    calculateEnemyDirection(enemy);
  });
}

function placeWave(gameState, point) {
  let { waves, towerList, roadList, obstacleList, enemyList, startPoint, endPoint } = gameState;
  if (!isGridFree([...towerList, ...roadList, ...obstacleList], enemyList, startPoint, endPoint, point)) {
    return;
  }
  let wave = { ...gameState.waves, gridX: point.gridX, gridY: point.gridY };

  if (gameState.wave) {
    waves.push(wave);
    enemyList.forEach((enemy) => {
      makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint);
      calculateEnemyDirection(enemy);
    });
  }
}

function placeObstacle(gameState, point) {
  let { towerList, roadList, obstacleList, enemyList, startPoint, endPoint } = gameState;
  if (!isGridFree([...towerList, ...roadList, ...obstacleList], enemyList, startPoint, endPoint, point)) {
    return;
  }
  let obstacle = { ...gameState.obstacle, gridX: point.gridX, gridY: point.gridY };

  if (gameState.obstacle) {
    if (callAStar(towerList, roadList, [...obstacleList, obstacle], maxX, maxY, startPoint, endPoint) === null) {
      return;
    }
    obstacleList.push(obstacle);
    enemyList.forEach((enemy) => {
      makeEnemyPath(enemy, towerList, roadList, obstacleList, endPoint);
      calculateEnemyDirection(enemy);
    });
  }
}

function isGridFree(towerList, enemyList, startPoint, endPoint, point) {
  if (compareGrids(point, startPoint) || compareGrids(point, endPoint)) {
    return false;
  }
  if (
    towerList.filter((tower) => {
      return compareGrids(tower, point);
    }).length > 0
  ) {
    return false;
  }
  if (
    enemyList.filter((enemy) => {
      return compareGrids(coordsToGrid(enemy.x, enemy.y), point);
    }).length > 0
  ) {
    return false;
  }
  return true;
}

function compareGrids(point1, point2) {
  return point1.gridX === point2.gridX && point1.gridY === point2.gridY;
}

function makeEnemy(x, y, speed, size, direction) {
  return {
    x,
    y,
    speed,
    size,
    direction,
  };
}

function makeWave(beginTime, enemiesCounter, enemy, repeatTime, lastDeployTime) {
  return {
    beginTime,
    enemiesCounter,
    enemy,
    repeatTime,
    lastDeployTime,
  };
}

let t;
let gameSpeed = 2;
let timer_is_on = 1;

function makeGameSpeedSlower() {
  if (gameSpeed > 0.2) {
    gameSpeed = (gameSpeed * 10 - 1) / 10;
    gameSpeed.toFixed(1);
  }
}
function makeGameSpeedFaster() {
  if (gameSpeed < 5) {
    gameSpeed = (gameSpeed * 10 + 1) / 10;
    gameSpeed.toFixed(1);
  }
}

function startCounter(fps, gameState, canvas, editor) {
  fpsInterval = 1000 / fps;
  then = gameState.gameStartTime;

  animate(gameState, canvas, editor, fps);
}

function animate(gameState, canvas, editor, fps) {
  if (editor) {
    draw(gameState, canvas, editor);
    requestAnimationFrame(() => animate(gameState, canvas, editor, fps));
    return;
  }

  if (!editor) {
    let gamerScore = JSON.parse(localStorage.getItem('HighScore') || '{}');
    if (!gamerScore[gameState.name] || gamerScore[gameState.name] < gameState.score) {
      gamerScore[gameState.name] = gameState.score;
      localStorage.setItem('HighScore', JSON.stringify(gamerScore));
    }
    let now = Date.now();
    let elapsedTime = (now - gameState.lastUpdate) * timer_is_on * gameSpeed;
    if (elapsedTime > fpsInterval) {
      then = now - (elapsedTime % fpsInterval);
    }

    let fpsSpeedDown = fps / 100;

    for (let i = 0; i < elapsedTime - 1; ++i) {
      if (gameState.onplay) {
        update(gameState, canvas, fpsSpeedDown);
      }
    }

    draw(gameState, canvas);
    if (gameState.onplay) {
      requestAnimationFrame(() => animate(gameState, canvas, editor, fps));
    }
    gameState.lastUpdate = now;
  }
}

function checkShoot(enemyList, deadList, corpseList, bulletList) {
  function collision(enemy, bullet) {
    return (
      Math.abs(enemy.x - bullet.x) < enemy.size / 2 + bullet.size / 2 &&
      Math.abs(enemy.y - bullet.y) < enemy.size / 2 + bullet.size / 2
    );
  }

  function bulletEffect(enemy, bullet) {
    if (bullet) {
      let bulletType = bullet.type;
      if (enemy) {
        switch (bulletType) {
          case 'poisonball':
            {
              enemy.score = Math.floor((enemy.score *= 1.05));
              enemy.reward = Math.floor((enemy.reward *= 1.05));
            }
            break;
          case 'iceball':
            if (enemy.life > 1) {
              enemy.speed *= 0.9;
            }
            break;
          default:
            return;
        }
      }
    }
  }

  let remainingBulletList = [];

  for (let j = 0; j < bulletList.length; ++j) {
    let hit = false;
    for (let i = 0; i < enemyList.length; ++i) {
      if (collision(enemyList[i], bulletList[j])) {
        bulletEffect(enemyList[j], bulletList[i]);
        enemyList[i].life -= 1;
        hit = true;
        break;
      }
    }
    if (!hit && remainingBulletList.indexOf(bulletList[j]) === -1) {
      remainingBulletList.push(bulletList[j]);
    }
  }

  deadList = [];
  corpseList = [];

  enemyList.forEach((enemy) => {
    if (enemy.life < 1) {
      enemy.alive = false;
      enemy.playdead = false;
      corpseList.push(enemy);
      deadList.push(enemy);
    }
  });

  let survivorList = enemyList.filter((enemy) => {
    return enemy.life > 0;
  });

  return { survivorList, deadList, corpseList, remainingBulletList };
}
