let loginSwitch = document.getElementById('login');
let registerSwitch = document.getElementById('register');
let submitButton = document.getElementById('btn');

function registerShowUp() {
  loginSwitch.style.left = '-400px';
  registerSwitch.style.left = '50px';
  submitButton.style.left = '110px';
}

function loginShowUp() {
  loginSwitch.style.left = '50px';
  registerSwitch.style.left = '450px';
  submitButton.style.left = '0px';
}

let usersObject = [
  {
    username: 'alma',
    password: '1234',
  },
  {
    username: 'test',
    password: '1234',
  },
  {
    username: 'Sanyi',
    password: '1111',
  },
];

function loginUser() {
  let username = document.getElementById('username').value;
  let password = document.getElementById('password').value;

  for (let i = 0; i < usersObject.length; ++i) {
    if (username === usersObject[i].username && password === usersObject[i].password) {
      setCookie('username', username);
      window.location.href = 'mainpage/mainpage.html';
      return;
    }
  }

  alert('⛫ Tower Defense Notice ⛫' + '\n' + '\n' + 'Incorrect username or password,' + '\n');
}

function registerUser() {
  let registerUser = document.getElementById('newuser').value;
  let registerPassword = document.getElementById('newpassword').value;
  let succesRegister = false;
  if (registerUser && registerPassword) {
    let newUser = {
      username: registerUser,
      password: registerPassword,
    };

    for (i = 0; i < usersObject.length; ++i) {
      if (registerUser === usersObject[i].username) {
        alert('⛫ Tower Defense Notice ⛫' + '\n' + '\n' + 'That username is taken,' + '\n' + 'please choose another');
        return;
      } else if (registerPassword.length < 6 || registerPassword > 20) {
        alert(
          '⛫   Tower Defense Notice   ⛫' +
            '\n' +
            '\n' +
            'That password is too short,' +
            '\n' +
            'please choose a 6-20 character long one, and must contains at least 1 letter',
        );

        return;
      }
    }

    usersObject.push(newUser);
    succesRegister = true;

    for (let i = 0; i < usersObject.length; ++i) {
      console.log(usersObject[i]);
    }
    //
  }
  if (succesRegister) {
    loginShowUp();
  }
}

const today = new Date();
const expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // 30 days

function setCookie(name, value) {
  document.cookie = name + '=' + escape(value) + '; path=/; expires=' + expiry.toGMTString();
}
function putCookie(form) {
  setCookie('userName', form[0]);

  return true;
}
