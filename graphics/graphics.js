let gridOn = false;

const stoneOgre = {
  id: 'stoneOgre',
  width: 454.5,
  height: 427.5,
  src: '../image/enemies/stoneOgre4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const scorpion = {
  width: 201.5,
  height: 233.5,
  src: '../image/enemies/scorpion4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const armoredScorpion = {
  width: 201.5,
  height: 233.5,
  src: '../image/enemies/armoredscorpion.png',
  minFrame: 0,
  maxFrame: 19,
};

const axeOrc = {
  width: 389.5,
  height: 332,
  src: '../image/enemies/orc4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const greenOrc = {
  width: 340.5,
  height: 304,
  src: '../image/enemies/greenorc4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const blackWizard = {
  width: 397.5,
  height: 338,
  src: '../image/enemies/blackwizard4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const armoredOrc = {
  width: 297.5,
  height: 255,
  src: '../image/enemies/armoredOrc4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const blueDevil = {
  width: 310.5,
  height: 263.5,
  src: '../image/enemies/bluedevil4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const blackDevil = {
  width: 356.5,
  height: 300.5,
  src: '../image/enemies/blackdevil4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const ghost = {
  width: 479.5,
  height: 286.5,
  src: '../image/enemies/ghost4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const redZombie = {
  width: 303.5,
  height: 282.5,
  src: '../image/enemies/redzombie4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const greenZombie = {
  width: 352.5,
  height: 355.5,
  src: '../image/enemies/greenzombie4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const armoredDevil = {
  width: 358.5,
  height: 298.5,
  src: '../image/enemies/armoreddevil4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const gnome = {
  width: 263.5,
  height: 212.5,
  src: '../image/enemies/gnome4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const purpleImp = {
  width: 301.5,
  height: 255.5,
  src: '../image/enemies/purpleimp4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const whiteDiver = {
  width: 325.5,
  height: 276.5,
  src: '../image/enemies/whitediver4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const axeman = {
  width: 348.5,
  height: 292.5,
  src: '../image/enemies/axeman4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const greenDevil = {
  width: 318.5,
  height: 264.5,
  src: '../image/enemies/greendevil4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const greyOgre = {
  width: 278.5,
  height: 225.5,
  src: '../image/enemies/greyogre4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const runner = {
  width: 201.5,
  height: 233.5,
  src: '../image/enemies/runner4way.png',
  minFrame: 0,
  maxFrame: 19,
};

const jumper = {
  width: 201.5,
  height: 233.5,
  src: '../image/enemies/jumper.png',
  minFrame: 0,
  maxFrame: 19,
};

const fireball = {
  width: 143.416667,
  height: 134,
  src: '../image/bullet/fireballHorizontal.png',
};

const cannonball = {
  width: 276,
  height: 278,
  src: '../image/bullet/cannonball.png',
};

const iceball = {
  width: 143.416667,
  height: 134,
  src: '../image/bullet/iceball.png',
};

const poisonball = {
  width: 143.416667,
  height: 134,
  src: '../image/bullet/poisonball.png',
};

const road = {
  width: 66.875,
  height: 66.833333,
  src: '../image/road/Cobblestone.png',
  x: 2,
  y: 2,
};

const rock = {
  row: 3,
  column: 6,
  width: 240,
  height: 240,
  src: '../image/obstacle/rock.png',
};

const tree = {
  row: 3,
  column: 2,
  width: 291,
  height: 269,
  src: '../image/obstacle/tree.png',
};

const water = {
  width: 86,
  height: 86,
  src: '../image/obstacle/water.png',
};

const grass = {
  src: '../image/terrain/backgroundgrass.jpg',
};

const snow = {
  src: '../image/terrain/snow.jpg',
};

const fireballSprite = new Image();
fireballSprite.src = fireball.src;

const iceballSprite = new Image();
iceballSprite.src = iceball.src;

const poisonballSprite = new Image();
poisonballSprite.src = poisonball.src;

const scorpionImage = new Image();
scorpionImage.src = scorpion.src;

const armoredScorpionImage = new Image();
armoredScorpionImage.src = armoredScorpion.src;

const greenOrcImage = new Image();
greenOrcImage.src = greenOrc.src;

const stoneOgreImage = new Image();
stoneOgreImage.src = stoneOgre.src;

const axeOrcImage = new Image();
axeOrcImage.src = axeOrc.src;

const blackWizardImage = new Image();
blackWizardImage.src = blackWizard.src;

const armoredOrcImage = new Image();
armoredOrcImage.src = armoredOrc.src;

const blueDevilImage = new Image();
blueDevilImage.src = blueDevil.src;

const blackDevilImage = new Image();
blackDevilImage.src = blackDevil.src;

const ghostImage = new Image();
ghostImage.src = ghost.src;

const redZombieImage = new Image();
redZombieImage.src = redZombie.src;

const greenZombieImage = new Image();
greenZombieImage.src = greenZombie.src;

const armoredDevilImage = new Image();
armoredDevilImage.src = armoredDevil.src;

const gnomeImage = new Image();
gnomeImage.src = gnome.src;

const purpleImpImage = new Image();
purpleImpImage.src = purpleImp.src;

const whiteDiverImage = new Image();
whiteDiverImage.src = whiteDiver.src;

const axeManImage = new Image();
axeManImage.src = axeman.src;

const greenDevilImage = new Image();
greenDevilImage.src = greenDevil.src;

const greyOgreImage = new Image();
greyOgreImage.src = greyOgre.src;

const runnerImage = new Image();
runnerImage.src = runner.src;

const jumperImage = new Image();
jumperImage.src = jumper.src;

const background = new Image();
background.src = grass.src;

const roadImage = new Image();
roadImage.src = road.src;

const firetowerImage = new Image();
firetowerImage.src = '../image/towers/greenwindow.png';
const cannonTowerImage = new Image();
cannonTowerImage.src = '../image/towers/canon2.png';
const iceTowerImage = new Image();
iceTowerImage.src = '../image/towers/bluetower.png';
const poisonTowerImage = new Image();
poisonTowerImage.src = '../image/towers/tower.png';

const rockImage = new Image();
rockImage.src = rock.src;
const waterImage = new Image();
waterImage.src = water.src;
const treeImage = new Image();
treeImage.src = tree.src;

const cannonballImage = new Image();
cannonballImage.src = cannonball.src;

function showPlayToStart(turnOn, editor) {
  if (!editor) {
    let playSign = document.getElementById('presstoplay');
    if (!!turnOn === false) {
      playSign.innerHTML = 'Press Pause for break';
      playSign.style.display = 'none';
    } else {
      playSign.innerHTML = 'Game Paused, Press play to Start';
      playSign.style.display = 'block';
    }
  }
}

let targets = document.getElementsByClassName('target');
let controls = document.getElementsByClassName('controls');

function markSelection(elements) {
  for (let i = 0; i < elements.length; i++) {
    elements[i].onclick = function () {
      let elem = elements[0];
      while (elem) {
        if (elem.tagName === 'DIV') {
          elem.classList.remove('selected');
        }
        elem = elem.nextSibling;
      }
      this.classList.add('selected');
    };
  }
}
markSelection(targets);
markSelection(controls);

const editorBackground = new Image();
editorBackground.src = grass.src;

const gameBackground = new Image();
gameBackground.src = snow.src;

function draw(gameState, canvas, editor) {
  let ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if (editor) {
    drawBackground(ctx, editorBackground, maxX, maxY);
  } else {
    drawBackground(ctx, gameBackground, maxX, maxY);
  }

  if (gridOn) {
    grid(ctx, maxX, maxY);
  }
  roadDraw(ctx, gameState.roadList, gameState.selectedObject);
  obstacleDraw(ctx, gameState.obstacleList, gameState.selectedObject);
  towerDraw(ctx, gameState.towerList, gameState.selectedTower);
  enemyDraw(ctx, gameState.enemyList, gameState.deadList, gameState.lastUpdate);
  bulletDraw(ctx, gameState.bulletList, gameState.lastUpdate);

  document.getElementById('lives').innerHTML = '💓' + 'Lives: ' + gameState.livesRemaining;
  let money = document.getElementById('money');
  if (money) {
    money.innerHTML = '💰' + 'Money: ' + gameState.money;
  }
  let score = document.getElementById('score');
  if (score) {
    score.innerHTML = '🏆' + 'Score: ' + gameState.score;
  }
  if (!editor) {
    document.getElementById('poisonTower').innerHTML =
      '☠️' + 'Poison Tower:  ' + Math.floor(POISONTOWER_PRICE * gameState.difficulty) + ' 💰';
    document.getElementById('iceTower').innerHTML =
      '🧊' + 'Ice Tower: ' + Math.floor(ICETOWER_PRICE * gameState.difficulty) + '  💰';
    document.getElementById('cannonTower').innerHTML =
      '💣' + 'Cannon: ' + Math.floor(CANNONTOWER_PRICE * gameState.difficulty) + ' 💰';
    document.getElementById('flameTower').innerHTML =
      '🔥' + 'Flame Tower:  ' + Math.floor(FLAMETOWER_PRICE * gameState.difficulty) + '  💰';
  } else {
    document.getElementById('poisonTower').innerHTML = '☠️' + 'Poison Tower ';
    document.getElementById('iceTower').innerHTML = '🧊' + 'Ice Tower ';
    document.getElementById('cannonTower').innerHTML = '💣' + 'Cannon ';
    document.getElementById('flameTower').innerHTML = '🔥' + 'Flame Tower';
  }
  document.getElementById('gamespeed').innerHTML = '⏱️' + 'Game Speed: ' + gameSpeed;
  document.getElementById('enemies').innerHTML = '👥' + 'Enemies left: ' + gameState.remainingEnemies;

  document.getElementById('showGrid').addEventListener('click', function () {
    gridOn = !gridOn;
  });
}

function drawBackground(ctx, img, maxX, maxY) {
  ctx.drawImage(img, 0, 0, maxX * cellSize, maxY * cellSize);
}

function grid(ctx, maxX, maxY) {
  ctx.lineWidth = 1;
  for (let x = 0; x < maxX; x++) {
    for (let y = 0; y < maxY; y++) {
      ctx.beginPath();
      ctx.setLineDash([5, 15]);
      ctx.strokeStyle = '#bd3e08';
      ctx.strokeRect(cellSize * x + 0.5, cellSize * y + 0.5, cellSize, cellSize);
    }
  }
}

function towerDraw(ctx, towerList, selectedTower) {
  for (let i = 0; i < towerList.length; i++) {
    let towerName = towerList[i].towerName;

    switch (towerName) {
      case 'flameTower':
        ctx.drawImage(
          firetowerImage,
          towerList[i].gridX * cellSize,
          towerList[i].gridY * cellSize - cellSize / 2,
          cellSize,
          cellSize * 1.5,
        );
        break;
      case 'iceTower':
        ctx.drawImage(
          iceTowerImage,
          towerList[i].gridX * cellSize,
          towerList[i].gridY * cellSize - cellSize / 2,
          cellSize,
          cellSize * 1.5,
        );
        break;
      case 'poisonTower':
        ctx.drawImage(
          poisonTowerImage,
          towerList[i].gridX * cellSize,
          towerList[i].gridY * cellSize - cellSize / 2,
          cellSize,
          cellSize * 1.5,
        );
        break;
      case 'cannonTower':
        ctx.drawImage(
          cannonTowerImage,
          towerList[i].gridX * cellSize,
          towerList[i].gridY * cellSize - cellSize / 2,
          cellSize,
          cellSize * 1.5,
        );
        break;

      default:
        ctx.beginPath();
        ctx.ellipse(
          towerList[i].gridX * cellSize + cellSize / 2,
          towerList[i].gridY * cellSize + cellSize / 2,
          cellSize / 2,
          cellSize / 2,
          0,
          0,
          2 * Math.PI,
        );
        ctx.fill();
        ctx.closePath();
    }

    if (selectedTower === towerList[i]) {
      towerRangeDraw(ctx, towerList[i]);
    }
  }
  ctx.restore();
}

function towerRangeDraw(ctx, tower) {
  ctx.lineWidth = 2;
  ctx.strokeStyle = 'RED';
  ctx.globalAlpha = 1;
  ctx.beginPath();
  ctx.ellipse(
    tower.gridX * cellSize + cellSize / 2,
    tower.gridY * cellSize + cellSize / 2,
    tower.range * cellSize,
    tower.range * cellSize,
    0,
    0,
    2 * Math.PI,
  );
  ctx.stroke();
  ctx.globalAlpha = 1;
  ctx.closePath();
}

function selectedObjectDraw(ctx, object) {
  ctx.beginPath();
  ctx.lineWidth = '2';
  ctx.strokeStyle = 'WHITE';
  ctx.beginPath();
  ctx.rect(object.gridX * cellSize, object.gridY * cellSize, cellSize, cellSize);
  ctx.stroke();
  ctx.closePath();
}

function roadDraw(ctx, roadList, selectedRoad) {
  for (let i = 0; i < roadList.length; i++) {
    ctx.drawImage(
      roadImage,
      road.x,
      road.y,
      road.width * road.x,
      road.height * road.y,
      roadList[i].gridX * cellSize,
      roadList[i].gridY * cellSize,
      cellSize,
      cellSize,
    );

    if (selectedRoad === roadList[i]) {
      selectedObjectDraw(ctx, roadList[i]);
    }
  }
}

function randomizator(number) {
  return Math.floor(Math.random() * number);
}

let randomsrowArray = [];
for (let i = 0; i < randomsrowArray.length; ++i) {
  randomsrowArray.push(randomrow);
}
let randomcol = randomizator(rock.column);
let randomrow = randomizator(rock.row);
function obstacleDraw(ctx, obstacleList, selectedObstacle) {
  for (let i = 0; i < obstacleList.length; i++) {
    if (obstacleList[i].name === 'rock') {
      ctx.drawImage(
        rockImage,
        rock.width * obstacleList[i].randomRockRow,
        rock.height * obstacleList[i].randomRockCol,
        rock.width,
        rock.height,
        obstacleList[i].gridX * cellSize,
        obstacleList[i].gridY * cellSize,
        cellSize,
        cellSize,
      );
    }
    if (obstacleList[i].name === 'water') {
      ctx.drawImage(
        waterImage,
        water.width * 1,
        water.height * 1,
        water.width,
        water.height,
        obstacleList[i].gridX * cellSize,
        obstacleList[i].gridY * cellSize,
        cellSize,
        cellSize,
      );
    }

    if (obstacleList[i].name === 'tree') {
      ctx.drawImage(
        treeImage,
        tree.width * obstacleList[i].randomRockRow,
        tree.height * obstacleList[i].randomRockCol,
        tree.width,
        tree.height,
        obstacleList[i].gridX * cellSize,
        obstacleList[i].gridY * cellSize,
        cellSize,
        cellSize,
      );
    }

    if (selectedObstacle === obstacleList[i]) {
      selectedObjectDraw(ctx, obstacleList[i]);
    }
  }
}

function drawLife(ctx, x, y, life, maxLife) {
  ctx.save();
  ctx.strokeRect(x - 5, y - 10, 10, 2);
  ctx.fillStyle = 'GREEN';
  ctx.fillRect(x - 5, y - 10, (10 * life) / maxLife, 2);
  ctx.restore();
}

function animateEnemy(ctx, enemy, enemyImage, enemyData) {
  ctx.drawImage(
    enemyImage,
    enemy.width * enemyData.frameXCounter * timer_is_on,
    enemy.height * enemyData.frameYCounter,
    enemy.width,
    enemy.height,
    enemyData.x - cellSize,
    enemyData.y - cellSize,
    cellSize * 2,
    cellSize * 2,
  );

  if (enemyData.frameXCounter < enemy.maxFrame) {
    enemyData.frameXCounter += 1;
  } else {
    enemyData.frameXCounter = enemy.minFrame;
  }
}

function animateDead(ctx, enemy, enemyImage, enemyData) {
  ctx.drawImage(
    enemyImage,
    enemy.width * enemyData.frameXCounter * timer_is_on,
    enemy.height * 2,
    enemy.width,
    enemy.height,
    enemyData.x - cellSize,
    enemyData.y - cellSize,
    cellSize * 2,
    cellSize * 2,
  );

  if (enemyData.frameXCounter < enemy.maxFrame) {
    enemyData.frameXCounter += 1;
  }
  ++enemyData.deadAnimationCounter;

  if (enemyData.frameXCounter === enemy.maxFrame && enemyData.deadAnimationCounter === 50) {
    enemyData.playdead = true;
  }
}

function enemyDraw(ctx, enemyList, deadList) {
  for (let i = 0; i < enemyList.length; i++) {
    let enemyType = enemyList[i].type;
    switch (enemyType) {
      case 'scorpion':
        animateEnemy(ctx, scorpion, scorpionImage, enemyList[i]);
        break;
      case 'armoredScorpion':
        animateEnemy(ctx, armoredScorpion, armoredScorpionImage, enemyList[i]);
        break;
      case 'axeOrc':
        animateEnemy(ctx, axeOrc, axeOrcImage, enemyList[i]);
        break;
      case 'greenOrc':
        animateEnemy(ctx, greenOrc, greenOrcImage, enemyList[i]);
        break;
      case 'stoneOgre':
        animateEnemy(ctx, stoneOgre, stoneOgreImage, enemyList[i]);
        break;
      case 'blackWizard':
        animateEnemy(ctx, blackWizard, blackWizardImage, enemyList[i]);
        break;
      case 'armoredOrc':
        animateEnemy(ctx, armoredOrc, armoredOrcImage, enemyList[i]);
        break;
      case 'blueDevil':
        animateEnemy(ctx, blueDevil, blueDevilImage, enemyList[i]);
        break;
      case 'blackDevil':
        animateEnemy(ctx, blackDevil, blackDevilImage, enemyList[i]);
        break;
      case 'ghost':
        animateEnemy(ctx, ghost, ghostImage, enemyList[i]);
        break;
      case 'redZombie':
        animateEnemy(ctx, redZombie, redZombieImage, enemyList[i]);
        break;
      case 'greenZombie':
        animateEnemy(ctx, greenZombie, greenZombieImage, enemyList[i]);
        break;
      case 'armoredDevil':
        animateEnemy(ctx, armoredDevil, armoredDevilImage, enemyList[i]);
        break;
      case 'gnome':
        animateEnemy(ctx, gnome, gnomeImage, enemyList[i]);
        break;
      case 'purpleImp':
        animateEnemy(ctx, purpleImp, purpleImpImage, enemyList[i]);
        break;
      case 'whiteDiver':
        animateEnemy(ctx, whiteDiver, whiteDiverImage, enemyList[i]);
        break;
      case 'axeman':
        animateEnemy(ctx, axeman, axeManImage, enemyList[i]);
        break;
      case 'greenDevil':
        animateEnemy(ctx, greenDevil, greenDevilImage, enemyList[i]);
        break;
      case 'greyOgre':
        animateEnemy(ctx, greyOgre, greyOgreImage, enemyList[i]);
        break;
      case 'runner':
        animateEnemy(ctx, runner, runnerImage, enemyList[i]);
        break;
      case 'jumper':
        animateEnemy(ctx, jumper, jumperImage, enemyList[i]);
        break;
    }
    drawLife(ctx, enemyList[i].x, enemyList[i].y, enemyList[i].life, enemyList[i].maxLife);
  }

  for (let i = 0; i < deadList.length; i++) {
    let deadType = deadList[i].type;
    switch (deadType) {
      case 'scorpion':
        animateDead(ctx, scorpion, scorpionImage, deadList[i]);
        break;
      case 'armoredScorpion':
        animateDead(ctx, armoredScorpion, armoredScorpionImage, deadList[i]);
        break;
      case 'axeOrc':
        animateDead(ctx, axeOrc, axeOrcImage, deadList[i]);
        break;
      case 'greenOrc':
        animateDead(ctx, greenOrc, greenOrcImage, deadList[i]);
        break;
      case 'stoneOgre':
        animateDead(ctx, stoneOgre, stoneOgreImage, deadList[i]);
        break;
      case 'blackWizard':
        animateDead(ctx, blackWizard, blackWizardImage, deadList[i]);
        break;
      case 'armoredOrc':
        animateDead(ctx, armoredOrc, armoredOrcImage, deadList[i]);
        break;
      case 'blueDevil':
        animateDead(ctx, blueDevil, blueDevilImage, deadList[i]);
        break;
      case 'blackDevil':
        animateDead(ctx, blackDevil, blackDevilImage, deadList[i]);
        break;
      case 'ghost':
        animateDead(ctx, ghost, ghostImage, deadList[i]);
        break;
      case 'redZombie':
        animateDead(ctx, redZombie, redZombieImage, deadList[i]);
        break;
      case 'greenZombie':
        animateDead(ctx, greenZombie, greenZombieImage, deadList[i]);
        break;
      case 'armoredDevil':
        animateDead(ctx, armoredDevil, armoredDevilImage, deadList[i]);
        break;
      case 'gnome':
        animateDead(ctx, gnome, gnomeImage, deadList[i]);
        break;
      case 'purpleImp':
        animateDead(ctx, purpleImp, purpleImpImage, deadList[i]);
        break;
      case 'whiteDiver':
        animateDead(ctx, whiteDiver, whiteDiverImage, deadList[i]);
        break;
      case 'axeman':
        animateDead(ctx, axeman, axeManImage, deadList[i]);
        break;
      case 'greenDevil':
        animateDead(ctx, greenDevil, greenDevilImage, deadList[i]);
        break;
      case 'greyOgre':
        animateDead(ctx, greyOgre, greyOgreImage, deadList[i]);
        break;
      case 'runner':
        animateDead(ctx, runner, runnerImage, deadList[i]);
        break;
      case 'jumper':
        animateDead(ctx, jumper, jumperImage, deadList[i]);
        break;
    }
  }
}

function animateBullet(ctx, bulletType, bulletImage, bulletData) {
  ctx.drawImage(
    bulletImage,
    bulletType.width * bulletData.frameXCounter * timer_is_on,
    0,
    bulletType.width,
    bulletType.height,
    bulletData.x - cellSize,
    bulletData.y - cellSize,
    cellSize,
    cellSize,
  );

  if (bulletData.frameXCounter < bulletType.maxFrame) {
    bulletData.frameXCounter += 1;
  } else {
    bulletData.frameXCounter = 0;
  }
}

function bulletDraw(ctx, bulletList) {
  for (let i = 0; i < bulletList.length; i++) {
    let bulletType = bulletList[i].type;

    switch (bulletType) {
      case 'cannonball':
        ctx.drawImage(cannonballImage, bulletList[i].x, bulletList[i].y, cellSize / 1.8, cellSize / 1.8);
        break;
      case 'iceball':
        animateBullet(ctx, iceball, iceballSprite, bulletList[i]);
        break;
      case 'poisonball':
        animateBullet(ctx, poisonball, poisonballSprite, bulletList[i]);
        break;
      case 'fireball':
        animateBullet(ctx, fireball, fireballSprite, bulletList[i]);
        break;
    }
  }
}
