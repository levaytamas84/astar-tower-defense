window.onload = function () {
  if (document.cookie.length === 0) {
    window.location.href = '../index.html';
  }
};

/* window.onload = function () {
  console.log(window.location);
  if (document.cookie.length === 0) {
    //string összehasonlítás   if(document.cookie.valueOf())
    window.location.href = '../index.html';
  }
}; */

const navSlide = () => {
  const burger = document.querySelector('.burger');
  const nav = document.querySelector('.nav-links');
  const navLinks = document.querySelectorAll('.nav-links li');

  burger.addEventListener('click', () => {
    nav.classList.toggle('nav-active');
  });

  navLinks.forEach((link, index) => {
    if (link.style.animation) {
      link.style.animation('');
    } else {
      link.getElementsByClassName.animation = `navLinkFade 0.5s ease forward ${index / 7 + 0.3}s`;
    }
  });
};

navSlide();
