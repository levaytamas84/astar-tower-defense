function aStarPathFinder(map, start, end) {
  let openSet = [start];
  let cameFrom = new Map();
  start.g = 0;

  function heuristic(point, end) {
   
     return distance(point, end);
    
  }

  function distance(a, b) {
    let dX = Math.abs(a.x - b.x);
    let dY = Math.abs(a.y - b.y);
    let newDistance = Math.sqrt(dX * dX +  dY* dY);
    if(a.road && b.road){
      return newDistance /2;
    }
    else return newDistance * 2000;
  }

  function removeFromArray(arr, elt) {
    for (let i = arr.length - 1; i >= 0; i--) {
      if (arr[i] == elt) {
        arr.splice(i, 1);
      }
    }
  }

  function giveNeighbor(map, current) {
    let neighbors = [];
    for (let i = 0; i < map.length; i++) {
      for (let j = 0; j < map[i].length; j++) {
        neighbors.push(map[i][j]);
      }
    }
    return neighbors.filter((n) => {
      return (
        n.x >= current.x - 1 &&
        n.x <= current.x + 1 &&
        n.y >= current.y - 1 &&
        n.y <= current.y + 1 &&
        n.free &&
        n !== current
      );
    });
  }

  function reconstructPath(cameFrom, current) {
    let totalPath = [current];
    while (cameFrom.get(current) != null) {
      current = cameFrom.get(current);
      totalPath.unshift(current);
    }
    return totalPath;
  }

  while (openSet.length > 0) {
    let winner = 0;
    for (let i = 1; i < openSet.length; i++) {
      if (openSet[i].f < openSet[winner].f) {
        winner = i;
      }
    }
    let current = openSet[winner];
    lastCheckedNode = current;

    // Did I finish?
    if (current === end) {
      return reconstructPath(cameFrom, current);
    }

    removeFromArray(openSet, current);

    let neighbors = giveNeighbor(map, current);
    for (let i = 0; i < neighbors.length; i++) {
      let neighbor = neighbors[i];

      let tempG = current.g + distance(neighbor, current);

      if (tempG < neighbor.g) {
        cameFrom.set(neighbor, current);

        neighbor.g = tempG;
        neighbor.f = neighbor.g + heuristic(neighbor, end);
        neighbor.previous = current;
        if (!openSet.includes(neighbor)) {
          openSet.push(neighbor);
        }
      }
    }
  }
  return null;
}

